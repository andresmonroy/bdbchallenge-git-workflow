# Full-stack developer challenge - Git workflow
### Author: Andres Yesid Monroy Gonzalez

## Hotfix
Create branch 'hotfix' from branch 'master'

```bash
git checkout -b hotfix
```

Update readme.md and commit changes

```bash
git add readme.md
git commit -m 'hotfix'
```

Merge hotfix into master

```bash
git checkout master
git merge --no-ff -m 'v1.0.1' hotfix
```

## Feature1
Create branch 'feature1' from branch 'master'

```bash
git checkout -b feature1
git commit -m 'feature1'
```

Merge feature1 into master

```bash
git checkout master
git merge --no-ff -m 'v1.1.0' feature1
```

## Feature2
Create branch 'feature2' from branch 'master'

```bash
git checkout -b feature2
```

Update readme.md and commit changes

```bash
git add readme.md
git commit -m 'feature2 commit #1'
```

Update again readme.md and commit changes

```bash
git add readme.md
git commit -m 'feature2 commit #2'
```
Merge feature2 into master

```bash
git checkout master
git merge --no-ff -m 'v1.2.0' feature2
```

If there are conflicts, resolve this and commit changes

```bash
git add readme.md
git commit -m 'v1.2.0'
```
